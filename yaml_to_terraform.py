import yaml
from urllib.request import urlopen

# Default Git URL structure for retrieving Terraform code
git_url_template = "https://gitlab.com/automation_poc/[app_name]_tf_module/raw/master/tf_code/[app_name]_module.tf"

# Create new Terraform file to be applied
dynamic_terraform = open("env.tf", "w")

# Define ENV variables to be used
# TODO: Standardize ENV vars across projects/teams
dynamic_terraform.write("# ENV VARS\n")
dynamic_terraform.write("variable \"env_team_name\" {}\n")
dynamic_terraform.write("variable \"env_sg_id\" {}\n")
dynamic_terraform.write("variable \"env_subnet_id\" {}\n")
dynamic_terraform.write("variable \"env_key_pair\" {}\n")
dynamic_terraform.write("\n\n")

# Open YAML config file
with open("env.yaml", "r") as input_file:
    env_config = yaml.load(input_file)
    list_index = 0

    # Loop through config entries and pull the appropriate
    # Terraform module code from Git repository
    for list_entry in env_config:
        # Retrieve application name
        app_name = env_config[list_index]['application']['name']

        # Insert application name into template URL
        tf_url = git_url_template.replace("[app_name]", app_name.lower())

        # Read in text from the remote Terraform code
        code_block = urlopen(tf_url).read()

        # Write the decoded Terraform code to file
        dynamic_terraform.write(code_block.decode() + "\n\n\n")

        # Increment list index
        list_index = list_index+1
